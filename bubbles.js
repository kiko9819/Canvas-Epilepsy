const canvas = document.querySelector('#bubbles');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

let context = canvas.getContext('2d');

let maxRadius = 40;
let minRadius = 15;

let figures = [];

let mouse = {
  x: undefined,
  y: undefined,
};

let colorArray = [
  "#B01953",
  "#7D3651",
  "#33262B",
  "#40383B",
  "#B593A0"
];
canvas.addEventListener('mousemove', function(event) {
  mouse.x = event.x;
  mouse.y = event.y;
});
window.addEventListener('resize', function() {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  init();
});

function Figure(x, y, dx, dy) {
  this.x = x;
  this.y = y;
  this.dx = dx;
  this.dy = dy;
}

function Rectangle(x, y, dx, dy, width, height) {
  Figure.call(this, x, y, dx, dy);
  this.width = width;
  this.height = height;

  this.draw = function() {
    context.rect(this.x, this.y, this.width, this.height);
    context.stroke();
  }
  this.update = function() {
    if (this.x + this.width > window.innerWidth || this.x < 0) {
      this.dx = -this.dx;
    }
    if (this.y + this.height > window.innerHeight || this.y < 0) {
      this.dy = -this.dy;
    }
    this.x += this.dx;
    this.y += this.dy;
    this.draw();
  }
}

function Circle(x, y, dx, dy, radius) {
  Figure.call(this, x, y, dx, dy);
  this.radius = radius;
  this.color = colorArray[Math.floor(Math.random() * colorArray.length)];
  this.minRadius = radius;

  this.draw = function() {
    context.beginPath();
    context.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    context.fillStyle = this.color;
    context.fill();
  }

  this.update = function() {
    if (this.x + this.radius > window.innerWidth || this.x - this.radius < 0) {
      this.dx = -this.dx;
    }
    if (this.y + this.radius > window.innerHeight || this.y - this.radius < 0) {
      this.dy = -this.dy;
    }
    this.x += this.dx;
    this.y += this.dy;
    this.draw();
    if (Math.abs(mouse.x - this.x) < 50 && Math.abs(mouse.y - this.y) < 50) {
      if (this.radius < maxRadius) {
        this.radius += 15;
      }
    } else if (this.radius > this.minRadius) {
      this.radius -= 1;
    }
  }
}

function init() {
  figures = [];
  for (var i = 0; i < 500; i++) {
    let radius = Math.random() * 3 + 1;
    let x = Math.random() * (window.innerWidth - radius * 2) + radius;
    let y = Math.random() * (window.innerHeight - radius * 2) + radius;
    let dx = Math.random() * 5;
    let dy = Math.random() * 5;
    let width = Math.random() * 200 + 200;
    let height = Math.random() * 200 + 200;

    figures.push(new Circle(x, y, dx, dy, radius));
    figures.push(new Rectangle(x, y, dx, dy, width, height));
  }
}

function animate() {
  requestAnimationFrame(animate);
  context.clearRect(0, 0, innerWidth, innerHeight);
  for (var i = 0; i < figures.length; i++) {
    figures[i].update();
  }
}
animate();
init();
